import numpy as np
import tensorflow as tf
from keras import backend as K
from keras import Input, Model
from keras.layers import Dense, \
    Conv2D, Conv2DTranspose, Flatten, Reshape, Lambda
from matplotlib import pyplot as plt

from stacked_mnist import DataMode, StackedMNISTData
from keras.losses import binary_crossentropy
import sys

np.set_printoptions(threshold=sys.maxsize)
from tensorflow.keras import layers
import tensorflow_probability as tfp


class AutoEncoderConv:
    """
    Model for learning representations by mapping input to input
    Compresses into a representation of size N
    """

    def __init__(self, color, VAE, epochs):
        self.color = color
        self.VAE = VAE
        self.epochs = epochs

        input_model = Input(shape=(28, 28, 1))
        compressed_dim = 5

        # Encoder network:
        x = Conv2D(filters=32, kernel_size=3, activation="relu", strides=2, padding="same", use_bias=True)(input_model)
        x = Conv2D(filters=64, kernel_size=3, activation="relu", strides=2, padding="same", use_bias=True)(x)
        x = Flatten()(x)  # Flattens the input for the dense layer
        x = Dense(40, use_bias=True, activation="relu")(x)

        if self.VAE:
            self.z_mean = Dense(compressed_dim, name="Z_mean")(x)  # Mean of latent space Z
            self.z_log_var = Dense(compressed_dim, name="Z_log_var")(x)  # Log variance of latent space Z
            # Perform sampling from mean and log var of latent the space into Z
            z = Lambda(self.sampling, output_shape=(compressed_dim,), name="z")([self.z_mean, self.z_log_var])
            self.encoder_z = Model(inputs=input_model, outputs=[self.z_mean,
                                                                self.z_log_var])  # Create model for extracting mean and log var
        else:
            z = Dense(compressed_dim, activation="linear", use_bias=True)(x)  # Normal AE

        self.encoder = Model(input_model, z, name="Encoder")

        # Decoder network:
        input_decoder = Input(shape=(compressed_dim,))
        x = Dense(7 * 7 * 64, use_bias=True, activation="relu")(input_decoder)
        x = Reshape((7, 7, 64))(x)
        x = Conv2DTranspose(filters=64, kernel_size=3, activation='relu', strides=2, padding="same", use_bias=True)(x)
        x = Conv2DTranspose(filters=32, kernel_size=3, activation='relu', strides=2, padding="same", use_bias=True)(x)
        # Output layer
        x = Conv2DTranspose(filters=1, kernel_size=3, activation='sigmoid', padding="same", use_bias=True)(x)

        self.decoder = Model(input_decoder, x, name="Decoder")  # Create decoder network model
        self.auto_encoder = Model(input_model, self.decoder(self.encoder.outputs[0]), name="Auto_Encoder")

        if self.VAE:
            print("EAGER MODE:")
            print(tf.executing_eagerly())
            self.auto_encoder.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001), loss=self.elbo_loss)
        else:
            self.auto_encoder.compile(optimizer='adam', loss='binary_crossentropy')
        self.auto_encoder.summary()

        # self.decoder.summary()
        # self.encoder.summary()
        # self.auto_encoder.summary()


    def train_dataset(self, x_train, x_test):
        """
        If color images, train the model on each layer separately
        """
        if x_train.shape[-1] == 3:  # Color input, train each layer separately and divide epochs
            epoch_divided = int(self.epochs / 3)
            for i in range(3):
                self.auto_encoder.fit(x_train[:, :, :, i, None], x_train[:, :, :, i, None], epochs=epoch_divided, batch_size=25,
                                      validation_data=(x_test[:, :, :, i, None], x_test[:, :, :, i, None])
                                      , callbacks=PerformancePlotCallback(x_test, self, color=self.color))
        else:
            self.auto_encoder.fit(x_train, x_train, epochs=self.epochs, batch_size=25, validation_data=(x_test, x_test)
                                  , callbacks=PerformancePlotCallback(x_test, self, color=self.color))

    def predict(self, X_test):
        """
        If color images, predict on each color separately
        """
        if self.color:
            """
            Predicting for each color layer due to difficulty learning all three color layers in same model with VAE
            Regular AE works fine with color so i decided to train the model on the entire stacked images
            """
            preds = np.zeros(shape=X_test.shape)
            # [Samples, X, Y, Color]
            preds[:, :, :, 0, None] = self.auto_encoder.predict(X_test[:, :, :, 0, None])
            preds[:, :, :, 1, None] = self.auto_encoder.predict(X_test[:, :, :, 1, None])
            preds[:, :, :, 2, None] = self.auto_encoder.predict(X_test[:, :, :, 2, None])
            return preds
        else:
            return self.auto_encoder.predict(X_test)

    def decoder_predict(self, Z):
        """
        Generating the generative images of the representation stored in the compressed layer
        """
        if self.color:
            preds = np.zeros(shape=(Z.shape[0], 28, 28, 3))
            # [Samples,latent, Color]
            preds[:, :, :, 0, None] = self.decoder.predict(Z[:, :, 0, None])
            preds[:, :, :, 1, None] = self.decoder.predict(Z[:, :, 1, None])
            preds[:, :, :, 2, None] = self.decoder.predict(Z[:, :, 2, None])
            return preds
        else:
            return self.decoder.predict(Z)

    def save_weights(self, missing=False, color=False):
        if color:
            if self.VAE:
                if missing:
                    self.auto_encoder.save_weights(
                        'models/VAE_missing_C')
                else:
                    self.auto_encoder.save_weights(
                        'models/VAE_C')
            else:
                if missing:
                    self.auto_encoder.save_weights('models/autoencoder_missing_C')
                else:
                    self.auto_encoder.save_weights('models/autoencoder_C')
        else:
            if self.VAE:
                if missing:
                    self.auto_encoder.save_weights('models/VAE_missing')
                else:
                    self.auto_encoder.save_weights('models/VAE')
            else:
                if missing:
                    self.auto_encoder.save_weights('models/autoencoder_missing')
                else:
                    self.auto_encoder.save_weights('models/autoencoder')

    def load_weights(self, missing=False, color=False):
        if color:
            if self.VAE:
                if missing:
                    self.auto_encoder.load_weights('models/VAE_missing_C')
                else:
                    self.auto_encoder.load_weights('models/VAE_C')
            else:
                if missing:
                    self.auto_encoder.load_weights('models/autoencoder_missing_C')
                else:
                    self.auto_encoder.load_weights('models/autoencoder_C')
        else:
            if self.VAE:
                if missing:
                    self.auto_encoder.load_weights('models/VAE_missing')
                else:
                    self.auto_encoder.load_weights('models/VAE')
            else:
                if missing:
                    self.auto_encoder.load_weights('models/autoencoder_missing')
                else:
                    self.auto_encoder.load_weights('models/autoencoder')

    # Samples the z_mean and z_log_var distribution for input into Z
    def sampling(self, args):
        z_mean, z_log_var = args
        batch = K.shape(z_mean)[0]
        dim = K.int_shape(z_mean)[1]
        epsilon = K.random_normal(shape=(batch, dim))
        return z_mean + K.exp(0.5 * z_log_var) * epsilon

    def elbo_loss(self, true, pred):
        z_mean, z_log_var = self.encoder_z(true)
        rec_loss = tf.reduce_mean(
            tf.reduce_sum(
                tf.keras.losses.binary_crossentropy(true, pred), axis=(1, 2)
            )
        )
        kl_loss = self.kl_loss(z_mean, z_log_var)
        return rec_loss + kl_loss

    def kl_loss(self, mean, log_var):
        kl_loss = -0.5 * (1 + log_var - tf.square(mean) - tf.exp(log_var))
        return tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))


class PerformancePlotCallback(tf.keras.callbacks.Callback):
    """
    Callback for plotting progress
    """

    def __init__(self, x_test, ae, color):
        self.x_test = x_test
        self.ae = ae
        self.color = color

    def on_epoch_end(self, epoch, logs=None):
        """
        Display random sample from test set
        """
        x = np.random.randint(0, 1000)
        x_test = self.x_test[x:x + 1]
        f, axarr = plt.subplots(1, 2)
        axarr[0].imshow(x_test[0] * 255)
        if not self.color:
            plt.gray()
        axarr[1].imshow(self.ae.predict(x_test)[0])
        if not self.color:
            plt.gray()
        plt.show()
