import timeit

from matplotlib import pyplot as plt

from AutoEncoderConv import AutoEncoderConv
from sklearn.model_selection import train_test_split
from stacked_mnist import DataMode, StackedMNISTData
import numpy as np
import tensorflow as tf
from verification_net import VerificationNet
from mpl_toolkits.axes_grid1 import ImageGrid

stacked_mnist_mc = StackedMNISTData(DataMode.MONO_BINARY_COMPLETE)
stacked_mnist_mm = StackedMNISTData(DataMode.MONO_BINARY_MISSING)
stacked_mnist_cc = StackedMNISTData(DataMode.COLOR_BINARY_COMPLETE)
stacked_mnist_cm = StackedMNISTData(DataMode.COLOR_BINARY_MISSING)


def standard_ae(ae, color):
    if color:
        dataset_complete = stacked_mnist_cc
    else:
        dataset_complete = stacked_mnist_mc

    X_train, X_test, X_val, y_train, y_test, y_val = preprocess_dataset(dataset_complete)
    if train:
        ae.train_dataset(X_train, X_test)
        ae.save_weights(color=color, missing=False)
    else:
        ae.load_weights(color=color, missing=False)

    reconstructions = reconstruct_images(ae, X_val)
    display_n_images(reconstructions, color)
    print(v_net.check_predictability(reconstructions, y_val, tolerance=tolerance))
    print(v_net.check_class_coverage(reconstructions))


def generative_ae(ae, color):
    X_train, X_test, X_val, y_train, y_test, y_val = preprocess_dataset(stacked_mnist_mc)

    if train:
        ae.train_dataset(X_train, X_test)
        ae.save_weights(color=color)
    else:
        ae.load_weights(color=color)
    generative_test(ae)


def anomaly_ae(ae, color, n_samples=1500):
    loss_collection = []

    if color:
        X_train, X_test, X_val, y_train, y_test, y_val = preprocess_dataset(stacked_mnist_cm)
        X_train_c, X_test_c, X_val_c, y_train_c, y_test_c, y_val_c = preprocess_dataset(stacked_mnist_cc)
    else:
        X_train, X_test, X_val, y_train, y_test, y_val = preprocess_dataset(stacked_mnist_mm)
        X_train_c, X_test_c, X_val_c, y_train_c, y_test_c, y_val_c = preprocess_dataset(stacked_mnist_mc)

    if train:
        print("Training on missing dataset")
        ae.train_dataset(X_train, X_test)
        ae.save_weights(missing=True, color=color)
    else:
        ae.load_weights(missing=True, color=color)

    lossfunc = tf.keras.losses.BinaryCrossentropy(from_logits=True, label_smoothing=0)
    X_train_c = X_train_c[0:n_samples]  # Only sample n images from set

    if VAE:
        latent_shape = ae.decoder.layers[0].input_shape[-1][1]  # Extract compressed Z size
        if color:   # One Z for each color layer
            z = np.random.normal(0, 1, (n_samples, latent_shape, 3))  # Gauss distribution sample to be fed through decoder
        else:
            z = np.random.normal(0, 1, (n_samples, latent_shape))  # Gauss distribution sample to be fed through decoder
        representation = np.exp(ae.decoder_predict(z))      # Run sample Z vector through decoder (to get log P(X|Z))
                                                            # Then take the exponential to extract P(X|Z)
        for i in range(n_samples):
            loss = lossfunc(X_train_c[i], representation[i])   # Calculate cross entropy
            loss = tf.keras.backend.get_value(loss)
            loss_collection.append(loss)

        lowest_idx = np.argpartition(loss_collection, 25)       # Sort by lowest 25 probabilities of a pixel being present
        display_n_images(X_train_c[lowest_idx[0:25]], color, target=True)    # Display the lowest 25 probabilities
        print(y_train_c[lowest_idx[0:25]])   # Print the classes of the 25 lowest probabilities
    else:
        pred = ae.predict(X_train_c)
        pred[pred < 0.01] = 0

        lossfunc = tf.keras.losses.BinaryCrossentropy(from_logits=True, label_smoothing=0)
        for i in range(n_samples):
            loss = lossfunc(X_train_c[i], pred[i])
            loss = tf.keras.backend.get_value(loss)
            loss_collection.append(loss)

        lowest_idx = np.argpartition(loss_collection, -25)[-25:]       # Sort by highest 25 cross entropies
        display_n_images(X_train_c[lowest_idx[0:25]], color, target=True)    # Display the highest 25 cross entropies
        print(y_train_c[lowest_idx[0:25]])   # Print the classes of the highest 25 cross entropies


def preprocess_dataset(stacked_mnist_obj):
    X, y = stacked_mnist_obj.get_full_data_set()  # Returns (Sample, Target) touple

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=25)
    X_test, X_val, y_test, y_val = train_test_split(X_test, y_test, test_size=0.33, random_state=25)

    return X_train, X_test, X_val, y_train, y_test, y_val


def reconstruct_images(ae, x_test):
    decoded_imgs = ae.predict(x_test)

    # Below code is for plotting five target and predicion images
    plt.figure(figsize=(20, 4))
    for i in range(5):
        # Display original
        ax = plt.subplot(2, 5, i + 1)
        plt.imshow(x_test[i]*255)
        if not color:
            plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        # Display reconstruction
        ax = plt.subplot(2, 5, i + 1 + 5)
        plt.imshow(decoded_imgs[i])
        if not color:
            plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.show()

    return decoded_imgs


def generative_test(ae):
    """
    Run random values through the decoder part of the network
    To get the representations saved (and then displays it)
    """
    images = 25
    if VAE:
        if color:
            z = np.random.normal(0, 1, (images, 5, 3))
        else:
            z = np.random.normal(0, 1, (images, 5))
    else:
        if color:
            z = np.random.uniform(0, 1, (images, 5, 3))
        else:
            z = np.random.uniform(0, 1, (images, 5))

    generative = ae.decoder_predict(z)
    display_n_images(generative, color=color)
    print("Predictability / Quality:")
    print(v_net.check_predictability(generative, tolerance=tolerance))
    print("Class coverage:")
    print(v_net.check_class_coverage(generative, tolerance=tolerance))

def display_n_images(images, color, target = False):
    """
    Displays images in a 5x5 grid
    If target, the image is processed as binary and must be scaled by 255
    """
    n_images = images.shape[0]
    fig = plt.figure(figsize=(10., 10.))
    grid = ImageGrid(fig, 111,  # similar to subplot(111)
                     nrows_ncols=(5, 5),  # creates 2x2 grid of axes
                     axes_pad=0.1,  # pad between axes in inch.
                     )

    for ax, im in zip(grid, images):
        # Iterating over the grid returns the Axes.
        if target:
            ax.imshow(im*255)
        else:
            ax.imshow(im)

        if not color:
            plt.gray()

    plt.show()

def train_em_all(ae):
    # TODO: Note: Must be run individually, or else training is mixed.
    #print("TRAINING MONO COMPLETE")
    #X_train, X_test, X_val, y_train, y_test, y_val = preprocess_dataset(stacked_mnist_mc)
    #ae.train_dataset(X_train, X_test)
    #ae.save_weights(color=False, missing=False)
#
    #print("TRAINING MONO MISSING")
    #X_train, X_test, X_val, y_train, y_test, y_val = preprocess_dataset(stacked_mnist_mm)
    #ae.train_dataset(X_train, X_test)
    #ae.save_weights(color=False, missing=True)

    #print("TRAINING COLOR COMPLETE")
    #X_train, X_test, X_val, y_train, y_test, y_val = preprocess_dataset(stacked_mnist_cc)
    #ae.train_dataset(X_train, X_test)
    #ae.save_weights(color=True, missing=False)

    print("TRAINING COLOR MISSING")
    X_train, X_test, X_val, y_train, y_test, y_val = preprocess_dataset(stacked_mnist_cm)
    ae.train_dataset(X_train, X_test)
    ae.save_weights(color=True, missing=True)


if __name__ == '__main__':
    # Mode settings:
    color = False
    VAE = True

    # Training settings:
    train = False
    epochs = 100

    tolerance = 0.5 if color else 0.8   # For check predictability

    if tf.test.gpu_device_name() != '/device:GPU:0':
        print('WARNING: GPU device not found.')
    else:
        print('SUCCESS: Found GPU: {}'.format(tf.test.gpu_device_name()))

    v_net = VerificationNet()
    ae = AutoEncoderConv(color, VAE, epochs)

    """##### Applications of the Auto Encoder#####"""
    #standard_ae(ae, color)

    generative_ae(ae, color)

    #anomaly_ae(ae, color)
    """###########################################"""





