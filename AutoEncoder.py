import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from keras import Input
from keras.layers import Dense

from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.model_selection import train_test_split

from tensorflow.keras.models import Model

class AutoEncoder:

    """
    Model for learning representations by mapping input to input
    Compresses into a representation of size N

    Encoder should use convolutional 2D layer with stride > 1
    TODO tf.keras.layers.Conv2D
    Decoder should use transposed convolutional 2D layer
    TODO tf.keras.layers.Conv2DTranspose
    """

    def __init__(self):
        input_model = Input(shape=(784,))     # Creating symbolic tensor for input images of MNIST size (28x28)
        compressed_dim = 64

        encoding_network = Dense(compressed_dim, activation="relu")(input_model)
        decoding_network = Dense(784, activation='sigmoid')(encoding_network)

        # encoding_network = tf.keras.layers.Conv2D(filters=compressed_dim, kernel_size=8, activation="relu", strides=2)(input_model)
        # decoding_network = tf.keras.layers.Conv2DTranspose(filters=784, kernel_size=8, activation='sigmoid', strides=2)(encoding_network)

        self.auto_encoder = Model(input_model, decoding_network)  # Input and output

        # Separating the encoding network and decoding network for demonstration purposes:
        # Encoding network for showing encoded images
        self.encoder = Model(input_model, encoding_network)

        # Decoder for reconstructing from representation:
        compressed_input = Input(shape=(compressed_dim,))
        decoder_layer = self.auto_encoder.layers[-1]  # Getting the last layer

        self.decoder = Model(compressed_input, decoder_layer(compressed_input))

        self.auto_encoder.compile(optimizer='adam', loss='binary_crossentropy')

    def call(self, x_train, x_test):
        self.auto_encoder.fit(x_train, x_train, epochs=15, batch_size=10, validation_data=(x_test, x_test))

        #encoded = self.encoder(x)
        #decoded = self.decoder(encoded)
        #return decoded

    def predict(self, x_test):
        encoded_img = self.encoder.predict(x_test)
        decoded_img = self.decoder.predict(encoded_img)

        plt.figure(figsize=(20, 4))
        for i in range(5):
            # Display original
            ax = plt.subplot(2, 5, i + 1)
            plt.imshow(x_test[i].reshape(28, 28))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            # Display reconstruction
            ax = plt.subplot(2, 5, i + 1 + 5)
            plt.imshow(decoded_img[i].reshape(28, 28))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
        plt.show()
