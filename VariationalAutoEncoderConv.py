import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from keras import Input
from keras.layers import Dense, Conv2D, Conv2DTranspose, MaxPooling2D, UpSampling2D, Conv1D, Conv1DTranspose
from stacked_mnist import DataMode, StackedMNISTData
import sys
np.set_printoptions(threshold=sys.maxsize)

from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.model_selection import train_test_split


class VariationalAutoEncoderConv:

    """
    Model for learning representations by mapping input to input
    Compresses into a representation of size N
    """

    def __init__(self):
        input_model = Input(shape=(784,))     # Creating symbolic tensor for input images of MNIST size (28x28)

        self.auto_encoder = tf.keras.Sequential()
        # Encoder network:
        self.auto_encoder.add(Conv1D(filters=10, kernel_size=5, activation="relu", strides=2, padding="same", input_shape=(784, 1)))
        self.auto_encoder.add(Conv1D(filters=5, kernel_size=5, activation="relu", strides=2, padding="same"))
        # Decoder network:
        self.auto_encoder.add(Conv1DTranspose(filters=5, kernel_size=5, activation='relu', strides=2, padding="same"))
        self.auto_encoder.add(Conv1DTranspose(filters=10, kernel_size=5, activation='relu', strides=2, padding="same"))
        # Output layer
        self.auto_encoder.add(Conv1DTranspose(filters=1, kernel_size=1, activation='sigmoid', padding="same"))

        self.auto_encoder.compile(optimizer='adam', loss='binary_crossentropy')
        self.auto_encoder.build((784,))
        self.auto_encoder.summary()

        self.decoder = tf.keras.Sequential()
        self.decoder.add(Input(shape=(196, 5)))
        self.decoder.add(self.auto_encoder.layers[-3])
        self.decoder.add(self.auto_encoder.layers[-2])
        self.decoder.add(self.auto_encoder.layers[-1])
        self.decoder.compile(optimizer="adam", loss='binary_crossentropy')
        # self.auto_encoder.build((784,))
        self.decoder.summary()

        self.stacked_mnist_mc = StackedMNISTData(DataMode.MONO_BINARY_COMPLETE)
        self.stacked_mnist_mm = StackedMNISTData(DataMode.MONO_BINARY_MISSING)
        self.stacked_mnist_cc = StackedMNISTData(DataMode.COLOR_BINARY_COMPLETE)
        self.stacked_mnist_cm = StackedMNISTData(DataMode.COLOR_BINARY_MISSING)

        self.stacked_mnist_mc.plot_example()  # Demonstration

    def standard_ae(self):
        X_train, X_test, X_val, y_train, y_test, y_val = self.preprocess_dataset(self.stacked_mnist_mc)
        self.train_dataset(X_train, X_test)
        self.reconstruct_images(X_val)

    def generative_ae(self):
        X_train, X_test, X_val, y_train, y_test, y_val = self.preprocess_dataset(self.stacked_mnist_mc)
        self.train_dataset(X_train, X_test)
        self.generative_test()

    def anomaly_ae(self):
        X_train, X_test, X_val, y_train, y_test, y_val = self.preprocess_dataset(self.stacked_mnist_mm)

        self.train_dataset(X_train, X_test)
        X_train_c, X_test_c, X_val_c, y_train_c, y_test_c, y_val_c = self.preprocess_dataset(self.stacked_mnist_mc)
        X_train_c = X_train_c[0:500]  # Cutoff
        pred = self.auto_encoder.predict(X_train_c)
        pred[pred < 0.01] = 0
        pred = np.reshape(pred, (500, 784))

        print(pred.shape)
        print(X_train_c.shape)

        lossfunc = tf.keras.losses.BinaryCrossentropy(from_logits=True, label_smoothing=0)
        losses = lossfunc(pred, X_train_c).numpy()

        print(y_train_c[losses > np.mean(losses)])



    def find_threshold(self, x_train):
        reconstructions = self.auto_encoder.predict(x_train)
        # provides losses of individual instances
        reconstruction_errors = tf.keras.losses.msle(reconstructions, x_train)
        # threshold for anomaly scores
        threshold = np.mean(reconstruction_errors.numpy()) + np.std(reconstruction_errors.numpy())
        return threshold

    def preprocess_dataset(self, stacked_mnist_obj, two_dim=False):
        X, y = stacked_mnist_obj.get_full_data_set()  # Returns (Sample, Target) touple

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=25)
        X_test, X_val, y_test, y_val = train_test_split(X_test, y_test, test_size=0.33, random_state=25)

        if not two_dim:     # Flattening into series of 1D arrays
            #####
            buf = []
            for x in X_train:
                buf.append(x.flatten())
            X_train = np.array(buf)
            #####
            buf = []
            for x in X_test:
                buf.append(x.flatten())
            X_test = np.array(buf)
            #####
            buf = []
            for x in X_val:
                buf.append(x.flatten())
            X_val = np.array(buf)

        return X_train, X_test, X_val, y_train, y_test, y_val


    def train_dataset(self, x_train, x_test):
        self.auto_encoder.fit(x_train, x_train, epochs=1, batch_size=25, validation_data=(x_test, x_test))

    def reconstruct_images(self, x_test):
        decoded_img = self.auto_encoder.predict(x_test)

        plt.figure(figsize=(20, 4))
        for i in range(5):
            # Display original
            ax = plt.subplot(2, 5, i + 1)
            plt.imshow(x_test[i].reshape(28, 28))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            # Display reconstruction
            ax = plt.subplot(2, 5, i + 1 + 5)
            plt.imshow(decoded_img[i].reshape(28, 28))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
        plt.show()

    def generative_test(self):
        """
        Run random values through the decoder part of the network
        To get the representations saved (and then displays it)
        """
        images = 5
        z = np.random.randn(images, 196, 5)

        generative = self.decoder.predict(z)
        print(generative.shape)
        plt.figure(figsize=(20, 4))
        for i in range(images):
            # Display reconstruction
            ax = plt.subplot(2, 5, i + 1 + 5)
            plt.imshow(generative[i].reshape(28, 28))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
        plt.show()


    def detect_anomaly(self):
        pass